Sample configuration files for:

SystemD: christcoind.service
Upstart: christcoind.conf
OpenRC:  christcoind.openrc
         christcoind.openrcconf
CentOS:  christcoind.init
OS X:    org.christcoin.christcoind.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
