
Debian
====================
This directory contains files used to package christcoind/christcoin-qt
for Debian-based Linux systems. If you compile christcoind/christcoin-qt yourself, there are some useful files here.

## christcoin: URI support ##


christcoin-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install christcoin-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your christcoin-qt binary to `/usr/bin`
and the `../../share/pixmaps/christcoin128.png` to `/usr/share/pixmaps`

christcoin-qt.protocol (KDE)

