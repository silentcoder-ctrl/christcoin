<h1 align="center">
christcoin Core [DOGE, Ð]  
<br/><br/>
<img src="https://static.tumblr.com/ppdj5y9/Ae9mxmxtp/300coin.png" alt="christcoin" width="300"/>
</h1>

<div align="center">

[![christcoinBadge](https://img.shields.io/badge/Doge-Coin-yellow.svg)](https://christcoin.com)
[![MuchWow](https://img.shields.io/badge/Much-Wow-yellow.svg)](https://christcoin.com)

</div>

Select language: EN | [CN](./README_zh_CN.md)

christcoin is a cryptocurrency like Bitcoin, although it does not use SHA256 as
its proof of work (POW). Taking development cues from Tenebrix and Litecoin,
christcoin currently employs a simplified variant of scrypt.

**Website:** [christcoin.com](https://christcoin.com)

## Installation 💻

Please see [the installation guide](INSTALL.md) for information about installing
christcoin Core.

### Such ports

christcoin Core by default uses port `22556` for peer-to-peer communication that
is needed to synchronize the "mainnet" blockchain and stay informed of new
transactions and blocks. Additionally, a JSONRPC port can be opened, which
defaults to port `22555` for mainnet nodes. It is strongly recommended to not
expose RPC ports to the public internet.

| Function | mainnet | testnet | regtest |
| :------- | ------: | ------: | ------: |
| P2P      |   22556 |   44556 |   18444 |
| RPC      |   22555 |   44555 |   18332 |

## Ongoing development - Moon plan 🌒

christcoin Core is an open source and community driven software. The development
process is open and publicly visible; anyone can see, discuss and work on the
software.

Main development resources:

* [Github Projects](https://github.com/christcoin/christcoin/projects) is used to
  follow planned and in-progress work for upcoming releases.
* [Github Discussion](https://github.com/christcoin/christcoin/discussions) is used
  to discuss features, planned and unplanned, related to both the development of
  the christcoin Core software, the underlying protocols and the DOGE asset.  
* [christcoindev subreddit](https://www.reddit.com/r/christcoindev/)

### Version strategy
Version numbers are following ```major.minor.patch``` semantics.

### Branches
There are 3 types of branches in this repository:

- **master:** Stable, contains the latest version of the latest *major.minor* release.
- **maintenance:** Stable, contains the latest version of previous releases, which are still under active maintenance. Format: ```<version>-maint```
- **development:** Unstable, contains new code for planned releases. Format: ```<version>-dev```

*Master and maintenance branches are exclusively mutable by release. Planned*
*releases will always have a development branch and pull requests should be*
*submitted against those. Maintenance branches are there for **bug fixes only,***
*please submit new features against the development branch with the highest version.*

## Contributing 🤝

If you find a bug or experience issues with this software, please report it
using the [issue system](https://github.com/christcoin/christcoin/issues/new?assignees=&labels=bug&template=bug_report.md&title=%5Bbug%5D+).

Please see [the contribution guide](CONTRIBUTING.md) to see how you can
participate in the development of christcoin Core. There are often
[topics seeking help](https://github.com/christcoin/christcoin/labels/help%20wanted)
where your contributions will have high impact and get very appreciation. wow.

## Communities 🚀🍾

You can join the communities on different social media.
To see what's going on, meet people & discuss, find the lastest meme, learn
about christcoin, give or ask for help, to share your project.

Here are some places to visit:

* [christcoin subreddit](https://www.reddit.com/r/christcoin/)
* [Dogeducation subreddit](https://www.reddit.com/r/dogeducation/)
* [Discord](https://discord.gg/christcoin)
* [christcoin Twitter](https://twitter.com/christcoin)

## Very Much Frequently Asked Questions ❓

Do you have a question regarding christcoin? An answer is perhaps already in the
[FAQ](doc/FAQ.md) or the
[Q&A section](https://github.com/christcoin/christcoin/discussions/categories/q-a)
of the discussion board!

## License - Much license ⚖️
christcoin Core is released under the terms of the MIT license. See
[COPYING](COPYING) for more information or see
[opensource.org](https://opensource.org/licenses/MIT)
